/* Repositories Nexus */
nexusRepoSnapshot = 'maven-snapshots'
nexusRepoRelease = 'maven-releases'

/* info pom.xml */
groupId = ''
artifactId = ''
artifactPath = ''
packaging = ''
version = ''

/* check RELEASE or SNAPSHOT */
isSnapshot = true

pipeline {
    agent any
    stages {
        stage('Get info from POM') {
            steps {
                script {
                    pom = readMavenPom file: 'pom.xml'
                    groupId = pom.groupId
                    artifactId = pom.artifactId
                    packaging = pom.packaging
                    version = pom.version
                    artifactPath = "target/${artifactId}-${version}.${packaging}"
                    isSnapshot = version.endsWith('-SNAPSHOT')
                }
                echo groupId
                echo artifactId
                echo packaging
                echo version
                echo artifactPath
                echo "isSnapshot: ${isSnapshot}"
            }
        }
        stage('Build and Test artifact') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                    reuseNode true
                }
            }
            steps {
                sh 'mvn clean package -B -ntp'
            }
            post {
                success {
                    jacoco()
                }
            }
        }

        /*
        Configurate SonarQube webhook for quality gate
        Administration > Configuration > Webhooks > Create
        The URL should point to your Jenkins server http://{JENKINS_HOST}/sonarqube-webhook/
        */
        stage('Sonarqube analysis') {
            agent {
                docker {
                    image 'maven:3.6.3-jdk-11'
                    reuseNode true
                }
            }
            steps {
                withSonarQubeEnv('sonarqube-local') {
                    sh 'mvn sonar:sonar -B -ntp'
                }
                timeout(time: 10, unit: 'MINUTES') {
                    waitForQualityGate abortPipeline: true
                }
            }
        }
        stage('Deploy Nexus') {
            steps {
                script {
                    if (isSnapshot) {
                        nexusRepository = "${nexusRepoSnapshot}"
                    } else {
                        nexusRepository = "${nexusRepoRelease}"
                    }

                    nexusPublisher nexusInstanceId: 'nexus-local',
                    nexusRepositoryId: "${nexusRepository}",
                    packages: [[$class: 'MavenPackage',
                    mavenAssetList: [[
                        classifier: '',
                        extension: '',
                        filePath: artifactPath]],
                    mavenCoordinate: [
                        artifactId: "${artifactId}",
                        groupId: "${groupId}",
                        packaging:"${packaging}",
                        version: "${version}"]
                        ]]
                }
            }
        }
        stage('Deployment Wildfly') {
            agent {
                docker {
                    image 'ansible/ansible-runner:1.4.7'
                    args '-u root'
                    reuseNode true
                }
            }
            steps {
                script {
                    filesByGlob = findFiles(glob: "target/*.${packaging}")
                    filesByGlob.each { item ->
                        artifactName = item.name
                        artifactPath = item.path
                        artifactExists = fileExists artifactPath
                        echo "File ${artifactPath} exist?: ${artifactExists}"
                        if (artifactExists) {
                            ansiblePlaybook(
                                playbook: "$env.WORKSPACE/playbook.yml",
                                inventory: "$env.WORKSPACE/hosts",
                                disableHostKeyChecking: true,
                                colorized: true,
                                credentialsId: 'ubuntu-oracle-sshkey',
                                extras: "-e artifact_path=${artifactPath} -e artifact_name=${artifactName}"
                            )
                        }
                    }
                }
            }
        }
    }
    post {
        always {
            echo 'Finished do cleanup'
            deleteDir()
        }
    }
}
